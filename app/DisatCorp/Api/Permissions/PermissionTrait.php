<?php namespace DisatCorp\Api\Permissions;

/**
 *  Just use it on a User Eloquent Model
 */

trait PermissionTrait {

    public function hasRole($name)
    {
        foreach($this->roles as $role) {
            if($role->name == $name)
                return true;
        }
        return false;
    }

    public function addRole($role)
    {
        return $this->roles()->attach($role);
    }

    public function revokeRole($role)
    {
        return $this->roles()->detach($role);
    }

    public function revokeAllRoles()
    {
        foreach($this->roles as $role) {
            $this->revokeRole($role);
        }
    }

    public function addRoles(array $roles)
    {
        foreach($roles as $role) {
            $this->addRole($role);
        }
    }

} 