<?php namespace DisatCorp\Forms;


use Laracasts\Validation\FormValidator;

class ImagesForm extends FormValidator {

    protected $rules = [
        'image' => 'required|image|max:1024',
    ];
} 