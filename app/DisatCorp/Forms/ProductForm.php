<?php namespace DisatCorp\Forms;

use Laracasts\Validation\FormValidator;

class ProductForm extends FormValidator
{
    protected $rules = [
        'name' => 'required|max:100',
        'price' => 'required|numeric',
    ];
} 