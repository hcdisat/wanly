<?php namespace DisatCorp\Forms;


use Laracasts\Validation\FormValidator;

class RegistrationForm  extends FormValidator {

    protected $rules = [
        'fname' => 'required|max:100',
        'lname' => 'required|max:100',
        'email' => 'required|email|max:200|unique:users',
        'password' => 'required|confirmed:password_confirmation',
        'username' => 'required|min:5|unique:users',
        'tel_one' => 'required',
        'address_one' => 'required',

    ];
}