<?php namespace DisatCorp\Notifications\Flash;


use Illuminate\Session\Store;

class FlashNotifier {

    private $session;

    function __construct(Store $session)
    {
        $this->session = $session;
    }

    public function error($message)
    {
        $this->setter($message, 'danger');
    }

    public function success($message)
    {
        $this->setter($message, 'success');
    }

    public function message($message)
    {
        $this->setter($message, 'info');
    }

    private function setter($message, $level)
    {
        $this->session->flash('notifier.message', $message);
        $this->session->flash('notifier.level', $level);
    }

} 