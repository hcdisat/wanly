<?php namespace DisatCorp\Api\Notifications;

use Illuminate\Support\Facades\Mail;

trait NotificationMailTrait {

    /**
     * @param $package
     * @param $view
     * @param null $subject
     */
    private static function mailer($package, $view, $subject = null)
    {
        $data = ['package' => $package];
        Mail::send($view, $data, function ($message) use ($package) {
            $message->to($package->user->email, $package->user->getFullname())
                ->subject($subject);
        });
    }
}