<?php

class OrderLinesController extends \BaseController {

	/**
	 * Store a newly created resource in storage.
	 * POST /orderlines
	 *
	 * @return Response
	 */
	public function store()
	{
        $cart = Cart::find(Input::get('cart_id'))
            ?: $cart = Cart::create(['user_id' => Auth::user()->id, 'current' => true]);

        $product = Product::find(Input::only('product_id'))->first();
        //dd($product->price);
        $line = [
            'product_id' => $product->id,
            'quantity' => 1,
            'line_price' => $product->price,
            'cart_id' => $cart->id,
        ];
        $orderLine = OrderLine::create($line);
        return Redirect::back()->withCart($cart);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /orderlines/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $line = OrderLine::find($id);
        if($line) {
            $multiplier = Input::get('multiplier');

            if($multiplier < $line->quantity) {
                $line->quantity--;
            } elseif($multiplier == $line->quantity or $multiplier === 1){
                $line->quantity = $multiplier;
            } else {
                $line->quantity++;
            }

            $line->line_price = $line->product()->first()->price * $line->quantity;
            $line->save();
            $cart =  Cart::find(Input::get('cart'));
            return ! $cart ?: $this->refreshOrderLines($cart);
        }
        return Redirect::back();
	}

    public function destroy($line)
    {

    }

    private function refreshOrderLines(Cart $cart) {
        return View::make('components.cart')->withCart($cart);
    }

}