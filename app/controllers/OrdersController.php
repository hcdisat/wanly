<?php

class OrdersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /orders
	 *
	 * @return Response
	 */
	public function index()
	{
        list($model, $total, $quantity, $product_ids) = $this->getViewData();

        $products = Product::all();
		return View::make('orders.index', compact('model', 'product_ids', 'products', 'total', 'quantity'));
	}

    public function orderList()
    {
        $orders = Order::whereUserId(Auth::user()->id)->get();
        return View::make('orders.list')->withOrders($orders);
    }

	public function checkOut()
	{
        $userCart = Cart::whereUserId(Auth::user()->id)
            ->whereCurrent(true)
            ->orderBy('id')
            ->first();

		return View::make('orders.dispatch')
            ->withCart($userCart);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /orders
	 *
	 * @return Response
	 */
	public function store()
	{
        $order = null;
        $cart = Cart::find(Input::get('cart_id'));
        if($cart) {
            $order = Order::find($cart->order_id);
        } else {
            $order = Order::create(array_merge(Input::all(), ['user_id' => Auth::user()->id]));
        }

        if($order){
            $order->addOrderLines(Input::get('orderLine'));
            $cart->current = false;
            $cart->order_id = $order->id;
            $cart->save();
            Order::onCheckoutOrder($order);
            return Redirect::route('orders.user.list');
        }
	}

    public function show($id)
    {
        $order = Order::find($id);
        $lines = $order->orderLines()->get();
        return $order
            ? View::make('orders.show', compact('order', 'lines'))
            : Redirect::back();
    }
	/**
	 * Show the form for editing the specified resource.
	 * GET /orders/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$order = Order::find($id);
        $cart = Cart::whereOrderId($order->id)->first();
        $cart->current = true;
        $cart->save();
        Return Redirect::route('orders.index');
	}

    public function allOrders()
    {
        $orders = Order::all();
        return View::make('orders.list')->withOrders($orders);
    }


	/**
	 * Remove the specified resource from storage.
	 * DELETE /orders/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

	}

    public function removeOrder($id)
    {
        $order = Order::find($id);
        if($order){
            Cart::whereOrderId($order->id)->delete();
            OrderLine::whereOrderId($order->id)->delete();
            $order->delete();
            return Redirect::back();
        }
    }

    public function changeStatus($orderId)
    {
        $order = Order::find($orderId);
        if($order){
            if($order->status_id == 1) {
                $order->status_id = 2;
            } else {
                $order->status_id = 1;
            }
            $order->save();

            return Redirect::back();
        }
    }

    public function deleteLine($line)
    {
        $line = OrderLine::find($line);
        if($line) $line->delete();
        return Redirect::back();
    }

    private function getViewData($model = 'Cart', $member = null, $id = null)
    {
        switch($model)
        {
            case 'Cart':
                $user_cart = Cart::whereUserId(Auth::user()->id)
                    ->whereCurrent(true)
                    ->orderBy('id')
                    ->first();
                return $this->getData($user_cart, $member);
                break;
            case 'Order':
                return $this->getData($order, $member);
                break;
        }

    }

    private function getData($model, $member = null)
    {
        if($model) {
            $total = 0;
            $quantity = $model->orderLines()->count();

            $lines = $model->orderLines()->get();
            foreach ($lines as $line) {
                $total += $line->product()->first()->price;
            }

            $product_ids = $model
                ? $model->first()->orderLines()->lists('product_id')
                : [];

            if($member) {
                $data = [
                    'model' => $model,
                    'total' => $total,
                    'quantity' => $quantity,
                    'product_ids' => $product_ids
                ];
                return $data[$member];
            }
            return [$model, $total, $quantity, $product_ids];
        }
        return [null, 0, 1, []];
    }

}