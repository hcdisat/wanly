<?php

class PagesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /pages
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('pages.index');
	}

	public function restaurant()
	{
		return View::make('pages.restaurant');
	}

	public function recipe($recipeId)
	{
        $recipe = Recipe::find($recipeId);
        return View::make('pages.recipes')
            ->withRecipe($recipe);
	}

	public function events($name)
	{
        $event = sprintf('events.%s', $name);
        return View::exists($event)
            ? View::make('pages.events')->withEvent($event)
            : Redirect::home();
	}

	public function menu()
	{
		return View::make('pages.menu');
	}

	public function contact()
	{
		return View::make('pages.contact');
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /pages/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function error()
	{
		return View::make('pages.error');
	}

}