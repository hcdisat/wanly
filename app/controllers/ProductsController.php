<?php

use \DisatCorp\Forms\ProductForm;

class ProductsController extends \BaseController {

    private $productForm;
    function __construct(ProductForm $productForm)
    {
        $this->productForm = $productForm;
    }

    /**
	 * Display a listing of the resource.
	 * GET /products
	 *
	 * @return Response
	 */
	public function index()
	{
        $products = Product::paginate(10);
		return View::make('products.index')->withProducts($products);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /products/create
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('products.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /products
	 *
	 * @return Response
	 */
	public function store()
	{
        $this->productForm->validate(Input::only('name', 'price'));
		Product::create(array_merge(Input::all(), ['image' => 'product.jpeg']));
        return Redirect::route('products.index');
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /products/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $product = Product::find($id);
        return $product
            ? View::make('products.edit')->withProduct($product)
            : Redirect::back();
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /products/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $this->productForm->validate(Input::all());
		$product = Product::find($id);
        if($product){
            $product->update(Input::all());
            return Redirect::route('products.index');
        }
        return Redirect::back();
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /products/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$product = Product::find($id);
        if($product){
            $product->delete();
            return Redirect::route('products.index');
        }
        return Redirect::back();
	}

}