<?php
use DisatCorp\Forms\LoginForm;

class SessionController extends \BaseController {

    function __construct(LoginForm $loginForm)
    {
        $this->loginForm = $loginForm;
    }

    /**
	 * Show the form for creating a new resource.
	 * GET /session/create
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('pages.login');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /session
	 *
	 * @return Response
	 */
	public function store()
	{
        $this->loginForm->validate($inputs = Input::only('username', 'password'));
        return Auth::attempt(array_merge($inputs, ['confirmed' => 1]))
            ? Redirect::back()
            : Redirect::back()
                ->withInput(Input::all())
                ->withErrors('Usuario ó Password Incorrectos');
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /session/
	 * @return Response
	 */
	public function destroy()
	{
		Auth::logout();
        Return Redirect::home();
	}
}