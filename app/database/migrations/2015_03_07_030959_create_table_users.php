<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('fname', 100);
			$table->string('lname', 100);
            $table->string('email', 200)->unique();
            $table->string('username', 50)->unique();
            $table->string('id_card', 30)->nullable();
            $table->string('credit_card', 30);
            $table->string('tel_one', 30);
            $table->string('tel_two', 30)->nullable();
            $table->string('mobile', 30)->nullable();
            $table->string('address_one');
            $table->string('address_two')->nullable();
			$table->string('password', 128);
            $table->boolean('confirmed')->default(false);
            $table->string('confirmation_code', 30)->nullable();
            $table->string('remember_token', 200)->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
