<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ProductsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();
        Product::truncate();

		/*foreach(range(1, 10) as $index)
		{
			Product::create([
                'name' => $faker->company(),
                'image' => 'http://placehold.it/150x150',
                'price' => (FLOAT)$faker->numberBetween(100, 1000)
			]);
		}*/

        Product::create([
            'name' => 'Cerdo con Ajonjoli',
            'image' => 'ajonjoli.jpg',
            'price' => (FLOAT)$faker->numberBetween(100, 1000)
        ]);
        Product::create([
            'name' => 'Cerdo con Salsa Agridulce',
            'image' => 'cerdoagri.jpg',
            'price' => (FLOAT)$faker->numberBetween(100, 1000)
        ]);
        Product::create([
            'name' => 'Cerdo Teriyaki',
            'image' => 'cerdoteri.jpg',
            'price' => (FLOAT)$faker->numberBetween(100, 1000)
        ]);
        Product::create([
            'name' => 'Ensalada Mixta Road',
            'image' => 'ensaladam.jpg',
            'price' => (FLOAT)$faker->numberBetween(100, 1000)
        ]);
        Product::create([
            'name' => 'Ensalada Ly',
            'image' => 'ensaldaly.jpg',
            'price' => (FLOAT)$faker->numberBetween(100, 1000)
        ]);
        Product::create([
            'name' => 'Pollo Agridulce',
            'image' => 'polloagri.jpg',
            'price' => (FLOAT)$faker->numberBetween(100, 1000)
        ]);
        Product::create([
            'name' => 'Pollo al Limón',
            'image' => 'pollolim.jpg',
            'price' => (FLOAT)$faker->numberBetween(100, 1000)
        ]);
        Product::create([
            'name' => 'Pollo Teriyaki',
            'image' => 'pollotera.jpg',
            'price' => (FLOAT)$faker->numberBetween(100, 1000)
        ]);
        Product::create([
            'name' => 'Pato Pekin',
            'image' => 'patop.jpg',
            'price' => (FLOAT)$faker->numberBetween(100, 1000)
        ]);
        Product::create([
            'name' => 'Rollitos de Pollo',
            'image' => 'rollito.jpg',
            'price' => (FLOAT)$faker->numberBetween(100, 1000)
        ]);
        Product::create([
            'name' => 'Tacos Chinos',
            'image' => 'tacoc.jpg',
            'price' => (FLOAT)$faker->numberBetween(100, 1000)
        ]);
        Product::create([
            'name' => 'Arroz Frito',
            'image' => 'arrozf.jpg',
            'price' => (FLOAT)$faker->numberBetween(100, 1000)
        ]);
        Product::create([
            'name' => 'Papas Fritas',
            'image' => 'papas.jpg',
            'price' => (FLOAT)$faker->numberBetween(100, 1000)
        ]);
        Product::create([
            'name' => 'Plátanos Fritos',
            'image' => 'platano.jpg',
            'price' => (FLOAT)$faker->numberBetween(100, 1000)
        ]);
        Product::create([
            'name' => 'Botella de Agua',
            'image' => 'botella.jpg',
            'price' => (FLOAT)$faker->numberBetween(100, 1000)
        ]);
        Product::create([
            'name' => 'Jugos Naturales',
            'image' => 'jugosnatu.jpg',
            'price' => (FLOAT)$faker->numberBetween(100, 1000)
        ]);
        Product::create([
            'name' => 'Cerveza de Lata',
            'image' => 'lata.jpg',
            'price' => (FLOAT)$faker->numberBetween(100, 1000)
        ]);

	}

}