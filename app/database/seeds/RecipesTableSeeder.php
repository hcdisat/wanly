<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class RecipesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		/*foreach(range(1, 10) as $index)
		{
			Recipe::create([
                'name' => $faker->name(),
                'preparation' => $faker->text()
			]);
		}*/

        Recipe::truncate();

        Recipe::create([
            'name' => 'Cerdo Agridulce',
            'view' => 'recipes.cerdoagri'
        ]);
        Recipe::create([
            'name' => 'Ensalda Ly',
            'view' => 'recipes.ensalada'
        ]);
        Recipe::create([
            'name' => 'Tacos Criollos',
            'view' => 'recipes.tacos'
        ]);

        Recipe::create([
            'name' => 'Cerdo teriyaki',
            'view' => 'recipes.cerdoteri'
        ]);

        Recipe::create([
            'name' => 'Ensalada Mixta',
            'view' => 'recipes.ensaladamix'
        ]);

	}

}