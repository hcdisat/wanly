<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class RolesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

        Role::create([
            'name' => 'owner',
            'description' => 'Site owners group'
        ]);

        Role::create([
            'name' => 'admin',
            'description' => 'Site Administrators group'
        ]);
        Role::create([
            'name' => 'user',
            'description' => 'Site clients group'
        ]);
	}
}