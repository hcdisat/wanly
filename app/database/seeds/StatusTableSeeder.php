<?php

// Composer: "fzaninotto/faker": "v1.3.0"
    use Faker\Factory as Faker;

    class StatusTableSeeder extends Seeder {

        public function run()
        {
            $faker = Faker::create();
            Status::truncate();

            Status::create([
                'name' => 'En Preparación',
            ]);
            Status::create([
                'name' => 'Entregada',
            ]);
        }
    }