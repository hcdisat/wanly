<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();
        $user = User::create([
            'fname' => 'Admin',
            'lname' => 'Root',
            'email' => 'admin@restaurantewangly.tk',
            'password' => 'Admin123*',
            'confirmed' => true,
            'username' => 'mperez',
            'id_card' => '06600985276',
            'tel_one' => $faker->phoneNumber(),
            'tel_two' => $faker->phoneNumber(),
            'mobile' => $faker->phoneNumber(),
            'address_one' => $faker->address(),
            'address_two' => $faker->address(),

        ]);
        $user->roles()->attach(1);
        $user->roles()->attach(2);
        $user->roles()->attach(3);
        /*foreach(range(1, 10) as $index)
		{
            $user = User::create([
                'fname' => $faker->firstName(),
                'lname' => $faker->lastName(),
                'email' => $faker->email(),
                'password' => 'secret',
                'confirmed' => true,
                'username' => $faker->userName(),
                'id_card' => $faker->creditCardNumber(),
                'tel_one' => $faker->phoneNumber(),
                'tel_two' => $faker->phoneNumber(),
                'mobile' => $faker->phoneNumber(),
                'address_one' => $faker->address(),
                'address_two' => $faker->address(),

            ]);
            $user->roles()->attach(3);
		}*/
	}
}