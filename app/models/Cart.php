<?php

class Cart extends \Eloquent {
	protected $fillable = ['user_id'];

    public function orderLines()
    {
        return $this->hasMany('OrderLine');
    }

    public function Order()
    {
        return $this->belongsTo('Order');
    }
}