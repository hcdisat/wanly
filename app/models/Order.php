<?php

class Order extends \Eloquent {

	protected $fillable = ['user_id', 'status', 'comment', 'status_id'];

    public static function boot()
    {
        parent::boot();

        static::saved(function($order){

            if($order->status->id == 2){
                self::onReadyStatusOrder($order);
            }

        });
    }

    public function addOrderLines(array $lineIds)
    {
        $this->deleteOrderLines();
        foreach($lineIds as $id) {
            $line = OrderLine::find($id);
            $line->order_id = $this->id;
            $line->save();
        }
    }

    public function deleteOrderLines()
    {
        if($this->orderLines()->get()->count()) {
            $this->orderLines()->get()->map(function($line){
                $line->order_id = null;
            });
        }
    }

    public function user() {
        return $this->belongsTo('User');
    }

    public function status() {
        return $this->belongsTo('Status');
    }

    public function orderLines()
    {
        return $this->hasMany('OrderLine');
    }

    public static function onReadyStatusOrder($order)
    {
        if($order)
        {
            self::mailer($order, 'emails.notifications.ready');
        }

    }

    public static function onCheckoutOrder($order)
    {
        if($order)
        {
            self::mailer($order, 'emails.notifications.checkout');
        }

    }

    private static function mailer($order, $view)
    {
        $lines = $order->orderLines()->get();
        $total = $lines->sum('line_price');
        $comments = $order->comment;
        $data = ['order' => $order, 'total' => $total, 'comments' => $comments, 'lines' => $lines];
        Mail::send($view, $data, function ($message) use ($order) {
            $message->to($order->user->email, $order->user->getFullname())
                ->subject('Su orden esta disponible');
        });
    }

}