<?php

class OrderLine extends \Eloquent {
	protected $fillable = ['product_id', 'quantity', 'order_id', 'line_price', 'cart_id'];

    public function product()
    {
        return $this->belongsTo('Product');
    }
}