<?php

class Product extends \Eloquent {
	protected $fillable = ['name', 'price', 'image'];

}