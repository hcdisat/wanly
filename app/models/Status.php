<?php

class Status extends \Eloquent {
	protected $fillable = ['name'];

    public static function getStatusByName($name)
    {
        return Status::whereName($name)
            ->get();
    }
}