<?php

    use DisatCorp\Api\Permissions\PermissionTrait;
    use Illuminate\Auth\UserTrait;
    use Illuminate\Auth\UserInterface;
    use Illuminate\Auth\Reminders\RemindableTrait;
    use Illuminate\Auth\Reminders\RemindableInterface;
    use \Illuminate\Database\Eloquent\SoftDeletingTrait;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait, PermissionTrait, SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

    protected $fillable = [
        'fname', 'lname', 'password', 'username',
        'email', 'confirmation_code', 'id_card', 'tel_one',
        'tel_two', 'mobile', 'address_one', 'address_two',
    ];

    protected $dates = ['deleted_at'];
    protected $softDelete = true;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }

    public function getFullName()
    {
        return "{$this->attributes['lname']}, {$this->attributes['fname']}";
    }

    public function roles()
    {
        return $this->belongsToMany('Role');
    }

}
