<?php

$path = app_path().'/routes/';

Route::get('/', [
    'as' => 'home',
    'uses' => 'PagesController@index'
]);

Route::get('test', function () {
    //$products_ids = Cart::whereUserId(Auth::user()->id)->first()->orderLines()->get(['product_id']);
    return Cart::whereUserId(2)->first()->orderLines()->lists('product_id');
});

Route::controller('password', 'RemindersController');

foreach(File::files($path) as $file) {
    require_once $file;
}
