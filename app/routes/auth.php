<?php

/**
 * Registration
 */
Route::get('/register', [
    'as' => 'registration.create',
    'uses' => 'RegistrationController@create'
]);
Route::post('/register', [
    'as' => 'registration.store',
    'uses' => 'RegistrationController@store'
]);

Route::get('register/verify/{confirmation_code}', [
    'as' => 'registration.confirmation',
    'uses' => 'RegistrationController@confirm'
]);