<?php

    Route::group(['before' => 'role:user'], function() {

        Route::resource('orders', 'OrdersController', [
            'only' => [
                'index', 'store', 'show', 'edit', 'destroy'
            ]
        ]);

        Route::get('orders/deleteline/{line_id}', [
            'as' => 'orders.delete.line',
            'uses' => 'OrdersController@deleteLine'
        ]);

        Route::get('orders/users/checkout', [
            'as' => 'orders.checkout',
            'uses' => 'OrdersController@checkout'
        ]);

        Route::get('orders/user/list', [
            'as' => 'orders.user.list',
            'uses' => 'OrdersController@orderList'
        ]);

        Route::put('orders/quantity/{olId}', [
            'as' => 'orders.line.quantity',
            'uses' => 'OrderLinesController@edit'
        ]);

        Route::post('orders/cart/add', [
            'as' => 'orders.cart.add',
            'uses' => 'OrderLinesController@store'
        ]);

        Route::delete('orders/cart/remove/{cart_id}', [
            'as' => 'orders.cart.remove',
            'uses' => 'OrderLinesController@destroy'
        ]);


    });

    Route::group(['before' => 'role:admin'], function(){
        Route::get('orders/change/status/{orderId}', [
            'as' => 'orders.change.status',
            'uses' => 'OrdersController@changeStatus'
        ]);

        Route::get('orders/all/orders', [
            'as' => 'orders.all.list',
            'uses' => 'OrdersController@allOrders'
        ]);

        Route::get('orders/users/orders/remove/{cart_id}', [
            'as' => 'orders.remove',
            'uses' => 'OrdersController@removeOrder'
        ]);

    });
