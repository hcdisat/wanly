<?php
Route::get('pages/restaurant', [
    'as' => 'pages.restaurant',
    'uses' => 'PagesController@restaurant'
]);

Route::get('pages/recipes/{recipeId}', [
    'as' => 'pages.recipes',
    'uses' => 'PagesController@recipe'
]);

Route::get('pages/events/{event}', [
    'as' => 'pages.event',
    'uses' => 'PagesController@events'
]);
Route::get('pages/menu', [
    'as' => 'pages.menu',
    'uses' => 'PagesController@menu'
]);
    Route::get('pages/contact', [
        'as' => 'pages.contact',
        'uses' => 'PagesController@contact'
    ]);

    Route::get('pages/notauthorize', [
        'as' => 'pages.error',
        'uses' => 'PagesController@error'
    ]);


