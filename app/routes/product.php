<?php

Route::group(['before' => 'role:admin'], function(){

    Route::resource('products', 'ProductsController', [
        'only' => [
            'index', 'store', 'create',
            'edit', 'update', 'destroy'
        ]]);

});