<?php

    Route::resource('session', 'SessionController', [
        'only' => [ 'create','store']
    ]);

    Route::get('logout', [
        'as' => 'session.logout',
        'uses' => 'SessionController@destroy@destroy'
    ]);