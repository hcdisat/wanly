{{Form::open(['route' => 'orders.store', 'class' => 'form'])}}
<legend>Checkout</legend>
@foreach($cart->orderLines()->get() as $line)
<div>		
	<ul class="cart-holder">
		<li>{{$line->product()->first()->name}}</li>
		<li>
		<input type="number" value="{{$line->quantity}}" class="multiplier" 
			data-dst-cart="{{$cart->id}}" min="1"
			data-dst-url="{{route('orders.line.quantity', $line->id)}}"/>
		</li>
		{{Form::hidden('orderLine[]', $line->id)}}
		<li>precio: $RD <span role='price'>{{$line->line_price or 1}}</span></li>
		<li><a href="{{route('orders.delete.line', $line->id)}}" class="delete_product">Quitar</a></li>		
	</ul>	
	<hr>
</div>
@endforeach	
<div class="">Total: $RD <span id="total">{{number_format($cart->orderLines()->get()->sum('line_price'), 2)}}</span></div><hr>
{{Form::label('Observaciones')}}
{{Form::hidden('cart_id', $cart->id)}}
{{Form::hidden('status_id', 1)}}
{{Form::textarea('comment', null, ['cols' => '45', 'rows' => 3])}}
{{Form::submit('Checkout', ['class' => 'submit confirmation', 'data-dst-message' => 'enviar'])}} <br><br><br><br>
{{Form::close()}}