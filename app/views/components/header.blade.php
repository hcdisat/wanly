  <div id="header">
  <h1><a href="{{route('home')}}">{{ HTML::image('images/logo.gif') }}</a></h1>    
  <ul id="nav">
    <li><a href="{{route('home')}}">INICIO</a> &nbsp;|&nbsp; </li>
    <li><a href="{{route('pages.restaurant')}}">RESTAURANTE</a> &nbsp;|&nbsp; </li>
    <li><a href="{{route('pages.menu')}}">MENU</a> &nbsp;|&nbsp; </li>
    <li><a href="#">RESERVACIONES</a> &nbsp;|&nbsp; </li>
    <li><a href="{{route('session.create')}}">ORDENAR</a> &nbsp;|&nbsp; </li> 
    <li><a href="{{route('pages.contact')}}">CONTACTENOS</a></li>
  </ul>
  <!-- end nav -->
  @include('components.welcome')
</div>
  <!-- end header -->