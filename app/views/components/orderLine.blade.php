<div class="row">
    <div class="datagrid">
        <table>
            <thead>
            <tr>
                <th>ID</th>
                <th>Plato</th>
                <th>Cantidad</th>
                <th>Precio</th>                
            </tr>
            </thead>
            <tfoot>
            <!--pagination-->      
            </tfoot>
            <tbody>            
            @foreach($lines as $line)
            <tr class="{{$order->id % 2 == 0 ?: 'alt'}}">
                <td>{{$line->id}}</td>
                <td>{{$line->product()->first()->name}}</td>
                <td>{{$line->quantity}}</td>
                <td>$RD {{number_format($line->line_price, 2)}}</td>               
            </tr>            
            @endforeach()
            </tbody>
        </table>
    </div>
</div>