
<div id="body">
  <div id="body-i">
    <h2 id="hspecials">      
      {{ HTML::image('images/title_menus.gif', 'Menu Especial') }}
    </h2>
    <div id="specials">
      <div style="background-image: url({{asset('images/picture_1.jpg')}})" class="imagebox">
        <h3>Cerdo Teriyaki</h3>
        <p> Es una receta sencilla de este plato típico de la cocina asiática.</p>
        <p class="readmore"><a href="{{route('pages.recipes', 4)}}">... más</a></p>
      </div>
      <div style="background-image: url({{asset('images/picture_2.jpg')}})" class="imagebox">
        <h3>Ensalada Ly</h3>
        <p> Ven y come de manera sana una Ensalada Ly, una deliciosa ensalada con garbanzos que te va a encantar.</p>
        <p class="readmore"><a href="{{route('pages.recipes', 2)}}">... más</a></p>
      </div>
      <div style="background-image: url({{asset('images/picture_3.jpg')}});" class="imagebox">
        <h3>Taco Criollo</h3>
        <p>Deliciosos taquitos de carne molida de res con cubitos de papa, servidos con lechuga y salsa de jitomate casera.</p>
        <p class="readmore"><a href="{{route('pages.recipes', 3)}}">... más</a></p>
      </div>
    </div>
    <!-- end specials -->
    <h2 id="hevents">      
      {{ HTML::image('images/title_events.gif', 'Special Events') }}
    </h2>
    <div id="events">
      <h3><a href="{{route('pages.event', 'birthday')}}">02.04.2014</a></h3>
      <p>Fiesta de Cumpleaños Angela Castro.</p>
      <p class="readmore"><a href="{{route('pages.event', 'birthday')}}">... más</a></p>
      <h3><a href="{{route('pages.event', 'graduation')}}">03.12.2014</a></h3>
      <p>Celebración de Graduación Julio Pared.</p>
      <p class="readmore"><a href="{{route('pages.event', 'graduation')}}">... más</a></p>
    </div>
    <!-- end events -->
    <div class="clear" id="spacer"></div>
  </div>
</div>
<!-- end body -->
<div id="hotstuff">
  <h2>    
    {{ HTML::image('images/title_hot.gif', 'Today\'s Hot') }}
  </h2>
  <div style="background-image: url({{asset('images/picture_4.jpg')}});" id="hot-one" class="imagebox">
    <h3>Cerdo con salsa Agridulce</h3>
    <p>Rompe con la tradición de comer siempre con un sabor, aquí puedes tener un 2X1. Cerdo Empanizado luego frito y finalmente servido con nuestra inigualable salsa agridulce, servido con arroz blanco… créeme.. Inigualable!.</p>
    <p class="readmore"><a href="{{route('pages.recipes', 1)}}">... más</a></p>    
  </div>
  <div style="background-image: url({{asset('images/picture_5.jpg')}});" id="hot-two" class="imagebox">
    <h3>Ensalada Mixta Road</h3>
    <p>Realmente Light! Brócoli, repollo chino, hongos, zanahoria, waterchestnut, babycorn, bamboo, apio todos estos vegetales acompañan las deliciosas lonjas de pechuga de pollo en una salsa de color blanco que preparamos aquí.</p>
    <p class="readmore"><a href="{{route('pages.recipes', 5)}}">... más</a></p>
  </div>
  
</div>
<div class="clear"></div>