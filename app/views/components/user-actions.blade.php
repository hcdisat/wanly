<div class="row">
	<div class="col-left">
		<p class="notify">
			Bienvenido {{Auth::user()->username}}
		</p>
		<ul>
			<li>{{link_to_route('orders.index', 'Comprar')}}</li>
			<li><a href="{{route('orders.user.list')}}">Mis Ordenes</a></li>
			@if(Auth::user()->hasRole('admin'))
				<li>{{link_to_route('orders.all.list', 'Todas las ordenes')}}</li>
				<li>{{link_to_route('products.index', 'Administrar Productos')}}</li>
				<!--li>@{{link_to_route('users.index', 'Todas las ordenes')}}</li-->
			@endif
			<li>{{link_to('password/remind/', 'Cambiar Contraseña')}}</li>
			<li>{{link_to_route('session.logout', 'Cerrar sesión')}}</li>			
		</ul>
	</div>
</div>