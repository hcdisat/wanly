<div id="welcome">
	<h1>
		{{ HTML::image('images/title_welcome.gif', 'Bienvenido') }}		
	</h1>
	<p></p>
    @if(isset($model))
    <hr/>
   <div class="col-right">
       <a href="{{route('orders.checkout')}}">
           <ul>
               <li><i class="fa fa-cutlery fa-lg"></i> ({{$model->orderLines()->get()->count()}})</li>
               <li><i class="fa fa-money fa-lg"></i> {{number_format($model->orderLines()->get()->sum('line_price'), 2)}}</li>
           </ul>
       </a>
   </div>
    @endif()
</div>