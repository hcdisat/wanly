<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">	
</head>
<body>
	<h1>Confirmar su Dirección de Correo</h1>
	<div>
		Gracias por crear una cuenta en <strong>Restaurant Wangly</strong>.
		Siga el link de más abajo para verificar su dirección de correo.
		<h2>"copie y pegue el link en la barra de direcciones de su navegar"</h2>
		{{link_to('register/verify/'. $confirmation_code)}}
	</div>
</body>
</html>