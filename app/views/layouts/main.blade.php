@extends('layouts.master')
@section('body')
	@include('components.header')
	@yield('content')	
@stop
