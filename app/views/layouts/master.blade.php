<!DOCTYPE html>

<html lang="en">
<head>
<title>Restaurante Wang-Ly</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
{{HTML::style('css/style.css')}}
{{HTML::style('css/Centro.css')}}
{{HTML::style('css/reset-min.css')}}
{{HTML::style('css/style.css')}}
{{HTML::style('bower_components/960-grid-system/code/css/960.css')}}
{{HTML::style('bower_components/font-awesome/css/font-awesome.min.css')}}
<style>
	.space {
		margin-left:15px; 		
	}
</style>
</head>
<body>
<div id="wrapper">  
  @yield('body')
  <div class="clear"></div>  
</div>
{{HTML::script('bower_components/jquery/dist/jquery.min.js')}}
{{HTML::script('js/app.js')}}
</body>
</html>