@extends('layouts.main')
@section('content')	
<div id="">
  <div class="row"> 
  	@yield('page-content')    
    <!-- end events -->
    <div class="clear" id="spacer"></div>
  </div>
</div>
<!-- end body -->
<div id="hotstuff">
  
  @yield('pagination')
</div>
<div class="clear"></div>	
@stop
