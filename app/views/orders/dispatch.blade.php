@extends('layouts.pages')
@section('page-content')
<div class="row">
	@include('components.cart', $cart)
</div>
<div class="row">
	<div class="container_3">
		<div class="grid_1">
			<a class="btn btn-default" href="{{route('orders.index')}}">
				<i class="fa fa-arrow-left "></i> Atras
			</a>
		</div>
	</div>
</div>
@stop