@extends('layouts.pages')
@section('page-content')
<div>
<div class="container_9">
        <ul>
            @forelse($products as $product)
            @if(!$isAdded = in_array($product->id, $product_ids))
            <div class="grid_4 estilo-borde">
                <li class="">
                    {{HTML::image("images/{$product->image}", null, ['height' => '150', 'width' => '150'])}}
                    <br/>
                    <div><i class="fa fa-cutlery fa-lg"></i> {{$product->name}}</div>
                    <div><i class="fa fa-money fa-lg"></i> $RD {{$product->price}}</div>
                    <div>
                        {{Form::open(['route' => 'orders.cart.add'])}}
                        {{Form::hidden('product_id', $product->id)}}
                        {{Form::hidden('cart_id', !isset($model) ? null : $model->id)}}
                        <button name="add" class="btn-naked {{ $isAdded ? 'added' : 'not-added' }}" type="submit">
                            <i class="fa {{ $isAdded ? 'fa-minus' : 'fa-plus' }} fa-lg"></i>
                        </button>
                        {{Form::close()}}
                    </div>
                    <!--div><input type="checkbox" name="select" data-dst-id="@{{$product->id}}"></div-->
                </li>
            </div>            
            @endif
            @empty
                <li>No Existen Productos</li>       
            @endforelse
        </ul>
    </div>
</div>
<hr>
<div class="clear"></div>
<div class="container_9" id="cartHolder">
    &nbsp;    
</div>
@stop