@extends('layouts.pages')
@section('page-content')
@if( !Auth::user()->hasRole('admin'))
    <p class="notify">Mis Ordenes</p>
@else
    <p class="notify">Todas las Ordenes</p>
@endif    
<div class="row">
    <div class="datagrid">
        <table>
            <thead>
            <tr>
                <th>ID</th>
                <th>Usuario</th>
                <th>Estado</th>
                <th>Creada</th>
                @if(Auth::user()->hasRole('admin'))
                    <th>Acciones</th>
                @endif 
            </tr>
            </thead>
            <tfoot>
            <!--pagination-->      
            </tfoot>
            <tbody>            
            @foreach($orders as $order)
            <tr class="{{$order->id % 2 == 0 ?: 'alt'}}">
                <td>{{ link_to_route('orders.show', $order->id, $order->id)}}</td>
                <td>{{$order->user->getFullName()}}</td>
                <td>
                    @if(Auth::user()->hasRole('admin'))
                    <a href="{{route('orders.change.status', $order->id)}}" data-dst-message='entregar' class="confirmation">
                            {{$order->status->name}}
                    </a>
                    @else
                        {{$order->status->name}}
                    @endif 
                </td>
                <td>{{$order->created_at}}</td>
                @if(Auth::user()->hasRole('admin'))
                <td>                                     
                    <a href="{{route('orders.edit', $order->id)}}"><i class="fa fa-pencil"></i></a> &nbsp;
                    <a class="confirmation" data-dst-message='eliminar' href="{{route('orders.remove', $order->id)}}"><i class="fa fa-minus"></i></a> &nbsp;
                </td>
                @endif
            </tr>            
            @endforeach()
            </tbody>
        </table>
    </div>
</div>
@stop