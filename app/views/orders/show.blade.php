@extends('layouts.pages')
@section('page-content')
<div class="row">
		@include('components.orderLine', $lines)
	</div>
<div class="row">
	<div class="row">
		<div class="datagrid">
			<table>
				<thead>
					<tr>					
						<th>Cliente</th>
						<th>Dirección</th>
						<th>Teléfono</th>
						<th>Tarjeta de Crédito</th>						
					</tr>
				</thead>
				<tfoot>
					
				</tfoot>
				<tbody>            
					<tr>						
						<td>{{$order->user->getFullName()}}</td>
						<td>{{$order->user->address_one}}</td>
						<td>{{$order->user->tel_one}}</td>
						<td>{{$order->user->credit_card}}</td>
					</tr>  
				</tbody>
			</table>
		</div>
	</div>		
</div>
<div class="row">
	<div class="row">
		<div class="datagrid">
			<table>
				<thead>
					<tr>
					<th>Orderen #</th>						
						<th>Estado</th>
						<th>Total</th>                
					</tr>
				</thead>
				<tfoot>
					<!--pagination-->      
				</tfoot>
				<tbody>            
					<tr>
						<td>{{$order->id}}</td>						
						<td>{{$order->status->name}}</td>
						<td>$RD {{number_format($lines->sum('line_price'), 2)}}</td>               
					</tr>  
				</tbody>
			</table>
		</div>
	</div>		
</div>
<div class="row">
	<div class="container_9">
		<div class="grid_5">
			<h1 class="notify">Observaciones</h1>
			<p>{{$order->comment}}</p>
		</div>
	</div>
</div>
<div class="row">
	<div class="container_3">
		<div class="grid_1">
			<a class="btn btn-default" href="{{route('orders.user.list')}}">
				<i class="fa fa-arrow-left "></i> Atras
			</a>
		</div>
	</div>
</div>
@stop