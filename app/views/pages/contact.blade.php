@extends('layouts.pages')
@section('page-content')
<div class="row">
    <form class="form">
        <div>
            <h1>CONTACTENOS :</h1>
            <label>
                <span>Nombre</span><input id="name" type="text" name="name" />
            </label>

            <label>
                <span>Email</span><input id="email" type="text" name="email" />
            </label>

            <label>
                <span>Asunto</span><input id="subject" type="text" name="subject" />
            </label>

            <label>
                <span>Sugerencias y/o comentarios:</span><textarea id="feedback" name="feedback"></textarea>
                <input type="submit" value="Enviar" class="submit" />
            </label>

        </div>
    </form>
</div>
<div style="font-family: Arial" id="é">	
	<div>
		{{HTML::image('images/mapa.jpg', null, ['width' => '327px'])}}
		 <br />
     	<p>Teléfonos: 809-533-2024 y 809-533-2025.</p>
     	<p>Dirección: Ave. Indepedencia Km 7, Miramar.</p>
	</div>
</div>
@stop