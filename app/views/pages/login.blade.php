@extends('layouts.pages')
@section('page-content')
@if( Auth::guest())
<div class="row">
{{Form::open(['route' => 'session.store', 'class' => 'form'])}}
<legend>Login</legend>	
		{{Form::label('username', 'Usuario')}}
		{{Form::text('username', Input::old('username'), ['required' => 'required'])}}		

		{{Form::label('password', 'Password')}}
		{{Form::password('password')}} <br>
		@if(Session::has('errors'))		
		<ul>		
			<li>Errores</li>	
			@foreach($errors->all() as $error)				
				<li><span class="text-danger">{{$error}}</span></li>
			@endforeach
		</ul>
		@endif
								
		{{link_to_route('registration.create', 'Registrarse')}} <br/>		
		<li>{{link_to('password/remind/', 'Olvidó su contraseña')}}</li>
		{{Form::submit('Registrar', ['class' => 'submit'])}}
{{Form::close()}}</div>
@else
@include('components.user-actions')
@endif()
@stop