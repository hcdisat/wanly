@extends('layouts.pages')
@section('title')
@parent    
@stop
@section('page-content')
<div class="container">
    <div class="row">       
        {{ Form::open(['role' => 'form', 'method' => 'post', 'class' => 'form']) }}                        
        <fieldset>
        <legend>Recupere su contraseña</legend>
            <div class="form-group">
                {{ Form::email('email', null, 
                    [                    
                    'placeholder' => 'E-mail', 
                    'required' => 'required',
                    'autofocus'
                    ]) 
                }}
                {{ $errors->first('email', '<span class="text-danger">:message</span>') }}
            </div>                                                                            
            {{Form::submit('Reset', ['class' => 'submit'])}}
        </fieldset>
        @if(Session::has('flash_message'))
        <div class="form-group">
            <p class="text-danger text-center">{{ Session::get('flash_message') }}</p>    
        </div>                            
        @endif     
        @if(Session::has('error'))
        <div class="form-group">
            <p class="text-danger text-center">{{ Session::get('error') }}</p>    
        </div>                            
        @endif     
        @if(Session::has('status'))
        <div class="form-group">
            <p class="text-danger text-center">{{ Session::get('status') }}</p>    
        </div>                            
        @endif                        
        {{ Form::close() }}            
</div>
</div>
@stop