@extends('layouts.master')
@section('title')
@parent    
@stop
@section('body')
    <div class="container">
        <div class="row">           
                        {{ Form::open(['role' => 'form', 'method' => 'post', 'class' => 'form']) }}       
                            {{Form::hidden('token', $token)}}                 
                            <fieldset>
                            <legend>Recupere su contraseña</legend>
                             <div class="form-group">
                                    {{ Form::email('email', null, 
                                        [                                            
                                            'placeholder' => 'E-mail', 
                                            'required' => 'required',
                                            'autofocus'
                                        ]) 
                                    }}
                                    {{ $errors->first('email', '<span class="text-danger">:message</span>') }}                                             
                                </div> 
                                <div class="form-group">
                                    {{ Form::password('password',                                             
                                            [                                                
                                                'placeholder' => 'Password',
                                                'required' => 'required'
                                            ])
                                    }} 
                                    {{ $errors->first('password', '<span class="text-danger">:message</span>') }}                                             
                                </div>
                                        <div class="form-group">
                                    {{ Form::password('password_confirmation',                                             
                                            [                                            
                                                'placeholder' => 'Una vez mas Password',
                                                'required' => 'required'
                                            ])
                                    }}                                             
                                </div>                                
                                {{Form::submit('Reset', ['class' => 'submit'])}}
                            </fieldset>
                        @if(Session::has('flash_message'))
                            <div class="form-group">
                                <p class="text-danger text-center">{{ Session::get('flash_message') }}</p>    
                            </div>                            
                        @endif     
                        @if(Session::has('error'))
                            <div class="form-group">
                                <p class="text-danger text-center">{{ Session::get('error') }}</p>    
                            </div>                            
                        @endif     
                        @if(Session::has('status'))
                            <div class="form-group">
                                <p class="text-danger text-center">{{ Session::get('status') }}</p>    
                            </div>                            
                        @endif                        
                        {{ Form::close() }}                    
        </div>
    </div>
@stop