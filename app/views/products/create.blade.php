@extends('layouts.products')
@section('page-content')
{{Form::open(['route' => 'products.store', 'class' => 'form'])}}
<legend>Registro</legend>	
		{{Form::label('name', 'Nombre')}}
		{{Form::text('name', Input::old('name'), ['required' => 'required'])}}
		{{ $errors->first('name', '<span class="text-danger">:message</span>') }}		
		<br/>

		{{Form::label('price', 'Precio')}}
		{{Form::number('price', Input::old('price'), ['required' => 'required'])}}
		{{ $errors->first('price', '<span class="text-danger">:message</span>') }}
		<br/>
				
		{{Form::submit('Crear', ['class' => 'submit'])}}
{{Form::close()}}</div>
@stop