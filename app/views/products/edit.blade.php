@extends('layouts.products')
@section('page-content')
{{Form::open(['class' => 'form', 'method' => 'PUT', 'route' => ['products.update', $product->id]])}}
<legend>Registro</legend>	
		{{Form::label('name', 'Nombre')}}
		{{Form::text('name', $product->name, ['required' => 'required'])}}
		{{ $errors->first('name', '<span class="text-danger">:message</span>') }}		
		<br/>

		{{Form::label('price', 'Precio')}}
		{{Form::number('price', $product->price, ['required' => 'required'])}}
		{{ $errors->first('price', '<span class="text-danger">:message</span>') }}
		<br/>
				
		{{Form::submit('Crear', ['class' => 'submit'])}}
{{Form::close()}}</div>
@stop