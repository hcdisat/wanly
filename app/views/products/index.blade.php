@extends('layouts.products')
@section('page-content')
<div class="row">
	<div class="container_3">
		<div class="grid_1">
			<a class="btn btn-default" href="{{route('orders.user.list')}}">
				<a href="{{route('products.create')}}"><i class="fa fa-plus"> Agregar</i></a> &nbsp;
			</a>
		</div>
	</div>
</div>
<div class="row">
    <div class="datagrid">
        <table>
            <thead>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Precio</th>
                <th>creado</th>
                <th>Aciones</th>
            </tr>
            </thead>
            <tfoot>
            <!--pagination-->      
            </tfoot>
            <tbody>            
            @foreach($products as $product)
            <tr class="{{$product->id % 2 == 0 ?: 'alt'}}">
                <td>{{$product->id}}</td>
                <td>{{$product->name}}</td>
                <td>$RD {{number_format($product->price, 2)}}</td>
                <td>{{$product->created_at}}</td>                
                 <td>                                                    
                <a href="{{route('products.edit', $product->id)}}"><i class="fa fa-pencil"></i></a> &nbsp;
                {{Form::open(['route' => ['products.destroy', $product->id], 'method' => 'delete'])}}
                	<button class="btn-naked confirmation" data-dst-message='eliminar' type="submit"><i class="fa fa-minus"></i></a></button>
                {{Form::close()}}                
            </td>
            </tr>            
            @endforeach()
            </tbody>
        </table>
    </div>
</div>
@stop
@section('pagination')
	{{$products->links()}}	
@stop