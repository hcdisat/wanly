<h1>Cerdo Agridulce</h1>
<div></div>
Ingredientes (4 comensales):<br />
<div></div>
1 Kg. de Solomillo de cerdo<br />
1 cebolla de verdeo<br />
1 puerro<br />
1 diente de ajo<br />
5cdas de jerez<br />
1/2 cda de vinagre de vino<br />
2 1/2 cdas de azúcar<br />
25 grs. de jengibre<br />
5 cdas de salsa de soja<br />
1 cdita de maicena<br />
<div></div>
Cómo hacer Cerdo Agridulce paso a paso:<br />
Cortar la carne en trozos con forma de dados, más o menos un bocado.
Para untar la carne: Poner el ajo en un mortero y machacar mezclando con la salsa de soja y el azúcar. Untar la carne con esta preparación. Tiene que hacerse con las manos para que penetre en la carne. Dejar sin usar una hora y media para que tome bien el sabor.
Calentar un poco de aceite en una sartén y dorar la carne, incorporar 1 ½ agua caliente y cocer hasta que este la carne. Limpiar el puerro, y cortar en rodajas, agregar a la carne.
Preparar la salsa: Mezclar el jerez con la maicena y el vinagre y agregar a la carne, dejar que se espese, rectificar la sal y servir.