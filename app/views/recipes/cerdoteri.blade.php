<h3>Cerdo teriyaki</h3>
 
 <div></div>
<li>
Esta elaboración es muy sana ya que esta parte del cerdo no es demasiado grasa, ademas si lo acompañamos con este delicioso y vistoso salteado conseguimos una receta completa perfecta para una cena en familia

<div></div>
   Ingredientes:

   
<div></div>
<br />Para el cerdo teriyaki
8 filetes de lomo de cerdo
3 cucharadas de salsa teriyaki
2 Cucharadas de salsa de soja
Sal
5 pimientas ( Mezcla de pimienta rosa, negra, verde, blanca y de Sechuan)
1/2 Cucharadita de aceite de sésamo (Opcional)
Aceite de oliva
<div>
<br />Para el salteado
1/4 Brocoli<br />
1/4 Repollo<br />
2 Cebolletas<br />
Aceite de oliva<br />
Semillas de sésamo tostadas<br />
<br />

<div></div>
 Elaboración:<br />
 
<div></div>
Tiempo de elaboración: 20 minutos + tiempo de macerado.<br />
1.- Cortamos los filetes de cerdo en tiras y los ponemos a macerar con la salsa de soja, la de teriyaki, sal, 5 pimientas molidas y el aceite se sésamo. Dejamos macerar mínimo 1 hora. Lo mejor es dejarlo de un día para otro.<br />

2.- Preparamos las verduras para el salteado. Cortamos el repollo y la cebolleta en juliana fina y el brocoli en ramas pequeñas.<br />

3.- Una vez tengamos el cerdo macerado lo escurrimos, reservamos el caldo. Secamos bien la carne con papel absorvente.<br />

4.- Ponemos en una sarten profunda un chorro de aceite y cuando esté bien caliente salteamos durante un par de minutos la carne hasta que dore exteriormente. Añadimos el liquido de la maceración y dejamos cocer unos minutos hasta que reduzca y espese un poco.<br />
</div>