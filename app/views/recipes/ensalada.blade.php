    <h1>Ensalda Ly</h1>

<div></div>
   Ingredientes:<br />
- 2 tazas de espinacas<br />

- 2 tazas de corazones de lechuga<br />

- 5 fresas<br />

- 1/2 taza de nuez pecana<br />

- 1 taza de queso panela en <br />

- 1/2 taza de cebolla morada fileteada<br />

- 1/2 kilogramo de pasta de hojaldre<br />

- Aceite en aerosol<br />

- 1 recipiente redondo metálico<br />

 
 <div></div>
Ingredientes para la vinagreta de frambuesa:<br />

- 1 cucharada de miel de abeja<br />

- 4 frambuesas<br />

- 1/2 taza de aceite de oliva<br />

- 1/4 de taza de vinagre de vino tinto<br />

 
  <div></div>
Preparación de la ensalada primavera en canasta de pan<br />

Primero corta tiras de pasta de un centímetro de grosor, colócalas en forma de red, rocia la parte exterior del recipiente con el aceite. Ponlo boca abajo y coloca la red de pasta encima del mismo. Hornea por 20 minutos a 160 grados, retira deja enfriar y quita la canasta de pan.

Corta el rabo de las fresas y pícalas en cuatro pedazos. Agrega a la canasta de pan la espinaca y la lechuga alternada, añade las fresas, el queso y las nueces.

Para preparar la vinagreta, vierte en un recipiente el vinagre, el aceite, la miel, las frambuesas, con una cuchara aplástalas, mezcla y rocia en la ensalada