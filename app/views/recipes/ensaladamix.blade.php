<h1>Ensalada Mixta</h1>
<div></div>
Ingredientes:<br />
<div></div>
- 1 lechuga iceberg<br />
- 1/2 escarola<br />
- 1 cebolla tierna dulce<br />
- 2 tomate grandes de ensalada<br />
- 8 espárragos 2 huevos<br />
- 4 latas de atún en aceite de oliva<br />
- 12 aceitunas verdes<br />
- 150 ml de aceite de oliva<br />
- 50 ml de vinagre de módena<br />
- pimienta negra molida<br />
- sal<br />
<div></div>
Cómo hacer ensalada mixta:<br />


 
Ponemos a hervir agua en una cazuela pequeña con un poco de vinagre y sal. Cuando hierva el agua cocemos en ella los dos huevos durante unos 12 minutos.

Cuando estén cocidos los pasamos por el chorro de agua frí­a, los pelamos y los cortamos en rodajas.

Lavamos y cortamos a grosso modo las lechugas.

Cortamos los tomates de ensalada a gajos.

Cortamos la cebolla en juliana fina.

En una ensaladera hacemos una base de lechuga, colocando las hojas de lechuga troceadas.

Bordeamos con los gajos de tomate. Colocamos las rodajas de huevo duro y esparcimos el atún desmigado por encima..

Añadimos las aceitunas y los espárragos cruzados.

Aparte mezclamos aceite de oliva, vinagre de módena, salpimentamos, batimos bien con ello aliñamos la ensalada.