<h1>Tacos Criollos</h1>
<div></div>
Ingredientes:<br />
aceite para freír, el necesario:<br />
1 taza de cebolla picada en juliana fina:<br />
1 taza de repollo en juliana bien fina:<br />
1 taza de zanahoria en juliana bien fina:<br />
1 taza de jamón cortado en juliana bien fina:<br />
1 taza de cebollino picado en juliana bien fina:<br />
2 dientes de ajo finamente picados:<br />
1 cda. de ajinomoto:<br />
3 cdas. de soya:<br />
2 cdas. de aceite de ajonjolí:<br />
3 cdas. de semilla de ajonjolí:<br />
24 cuadros de pasta de wantán:<br />
sal y pimienta al gusto:<br />
<div></div>
Preparación:<br />
1. En un wok bien caliente, vierta unas 2 cucharadas de aceite y saltee las cebollas, la zanahoria y el repollo. Seguidamente agregue el jamón, el ajo y el cebollino. Saltee por unos 8 minutos y retire del fuego. Agregue el ajinomoto, la salsa de soya, el aceite de ajonjolí y las semillas de ajonjolí. Salpimiente y reserve.:<br />
2. Coloque agua en un tazón y moje con las yemas de los dedos los bordes de cada cuadro de wantán. Seguidamente distribuya un poco del relleno preparado anteriormente en el centro del cuadro y enrolle. Trate de introducir las puntas del cuadro a mitad del proceso de envoltura y termine hasta obtener un taco chino compacto.:<br />
3. En una olla caliente con buena cantidad de aceite, fría los tacos hasta que se doren completamente. Retire y elimine el exceso de aceite. Sirva y acompañe con soya o salsa agridulce.:<br />