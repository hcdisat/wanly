@extends('layouts.pages')
@section('page-content')
	<div class="row">
		<p class="notify">
	Su Cuenta fué verificada. </p>
	<div class="col-left">
		<p>
		Usuario: {{$user->username}} <br>
		Correo: {{$user->email}} <br>

		Ya puede hacer login {{link_to_route('session.create', 'aquí')}}
	</p>
	</div>
	</div>
@stop