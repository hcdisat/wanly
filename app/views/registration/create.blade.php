@forelse($errors as $error)
	{{var_dump($error)}}
@empty
@endforelse
@extends('layouts.pages')
@section('page-content')
<div class="row">{{Form::open(['route' => 'registration.store', 'class' => 'form'])}}
<legend>Registro</legend>	
		{{Form::label('fname', 'Nombre')}}
		{{Form::text('fname', Input::old('id_card'), ['required' => 'required'])}}
		{{ $errors->first('fname', '<span class="text-danger">:message</span>') }}		
		<br/>

		{{Form::label('lname', 'Apellido')}}
		{{Form::text('lname', Input::old('lname'), ['required' => 'required'])}}
		{{ $errors->first('lname', '<span class="text-danger">:message</span>') }}
		<br/>

		{{Form::label('id_card', 'Cédula')}}
		{{Form::text('id_card', Input::old('id_card'), ['required' => 'required'])}}
		{{ $errors->first('id_card', '<span class="text-danger">:message</span>') }}
		<br/>

		{{Form::label('tel_one', 'Teléfono 1')}}
		{{Form::text('tel_one', Input::old('tel_one'), ['required' => 'required'])}}
		{{ $errors->first('tel_one', '<span class="text-danger">:message</span>') }}
		<br/>

		{{Form::label('tel_two', 'Teléfono 2 (Opcional)')}}
		{{Form::text('tel_two', Input::old('tel_two'))}}		

		{{Form::label('mobile', 'Celular (Opcional)')}}
		{{Form::text('mobile', Input::old('mobile'))}}		

		{{Form::label('address_one', 'Dirección 1')}}
		{{Form::text('address_one', Input::old('address_one'), ['required' => 'required'])}}
		{{ $errors->first('address_one', '<span class="text-danger">:message</span>') }}
		<br/>

		{{Form::label('address_two', 'Dirección 2 (Opcional)')}}
		{{Form::text('address_two', Input::old('address_two'))}}		

		{{Form::label('username', 'Usuario')}}
		{{Form::text('username', Input::old('username'), ['required' => 'required'])}}
		{{ $errors->first('username', '<span class="text-danger">:message</span>') }}
		<br/>

		{{Form::label('password', 'Password')}}
		{{Form::password('password', ['required' => 'required'])}}
		{{ $errors->first('password_confirmation', '<span class="text-danger">:message</span>') }}
		<br/>
				
		{{Form::label('password_confirmation', 'Repita su password')}}		
		{{Form::password('password_confirmation', ['required' => 'required'])}}		

		{{Form::label('email', 'Email')}}
		{{Form::email('email', Input::old('email'), ['required' => 'required'])}}
		{{ $errors->first('email', '<span class="text-danger">:message</span>') }}
		<br/>
		
		{{Form::submit('Registrar', ['class' => 'submit'])}}
{{Form::close()}}</div>
@stop