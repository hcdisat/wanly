$(function(){
    var cart = Cart();
    $('body').on('click', 'a[name=add]', cart.add);
    $('body').on('change', 'input.multiplier', cart.multiply);
    $('body').on('click', 'a.delete_product', cart.remove);
    $('body').on('click', 'span[role=price]', cart.updateTotal);
    $('.confirmation').on('click', function () {
        return confirm('¿Seguro quiere '+$(this).data('dst-message')+' esta orden?');
    });
});

var Cart = function(){

    var add = function() {

        var options = {
            url: $(this).data('dst-url')
        };

        $.ajax(options).done(function(data){
            if(data.error) {
                return false;
            } else {
                $('#cartHolder').after(data);
                var holder = parseFloat($('#total').html()).toFixed(2);
                holder = Number(parseFloat($('.cart-holder').find('span[role=price]').html()));
                console.log(holder)
                $('#total').html(holder);
            }
        });
    };

    var incresePrice = function() {
        var $this = $(this);
        var url = $this.data('dst-url');

        var data = {
            multiplier: $this.val(),
            cart: $this.data('dst-cart')
        };
        var options = {
            url: url,
            type: 'put',
            data: data
        };

        $.ajax(options).done(function(data){
            var row = $this.parents('.row');
            row.html(data);
        });
    };

    var removeProduct = function() {
        $(this).closest('ul').parent().remove();
    };

    var updateTotal = function() {
        var value = parseFloat($('#total').html() * $(this).html()).toFixed(2);
        console.log(value);
        $('#total').html(value);
    };

    return {
        add: add,
        multiply: incresePrice,
        remove: removeProduct,
        updateTotal: updateTotal
    };
};